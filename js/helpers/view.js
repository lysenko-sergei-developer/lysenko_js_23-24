define(function () {
  return {
    clearField: function () {
      let field = document.getElementById('text-field')
      field.value = ''
    },

    renderCross: function () {
      let cross = document.createElement('i')
      cross.className = 'fa fa-times'
      cross.setAttribute('aria-hidden', true)

      return cross
    },

    renderEdit: function () {
      let edit = document.createElement('i')
      edit.className = 'fa fa-pencil'
      edit.setAttribute('aria-hidden', true)

      return edit
    },

    renderСollectedItem: function (item) {
      let todosItem = document.createElement('li')
      todosItem.innerHTML = item
      todosItem.className = 'todos-item'

      let todosList = document.getElementById('todos-list')
      let wrapperItem = document.createElement('div')
      wrapperItem.className = 'todos-item-block'

      wrapperItem.appendChild(this.renderCross())
      wrapperItem.appendChild(todosItem)
      wrapperItem.appendChild(this.renderEdit())

      this.clearField()
      todosList.appendChild(wrapperItem)
    },

    renderStrike: function (item) {
      let strike = document.createElement('strike')
      let newLi = item.cloneNode(true)
      strike.appendChild(newLi)

      let parDiv = item.parentNode;
      parDiv.replaceChild(strike, item)
    },

    removeStrike: function (item) {
      let newLi = item.cloneNode(true)

      let parDiv = item.parentNode.parentNode
      parDiv.replaceChild(newLi, item.parentNode)
    },

    editItem: function (item) {
      item.style.display = 'none';
      let input = document.createElement('input')
      input.className = 'input-item'
      input.focus()

      let todo = item.parentNode.getElementsByClassName('todos-item')[0]
      input.value = todo.innerHTML

      let parDiv = item.parentNode;
      parDiv.replaceChild(input, todo)

      input.addEventListener('keydown', (e) => {
        if (e.which == 13) {
          todo.innerHTML = input.value
          item.style.display = 'block';
          parDiv.replaceChild(todo, input)
        }
      })
    },

    removeItem: function (item) {
      (item.parentNode.parentNode).removeChild(item.parentNode)
    },
  }
})
