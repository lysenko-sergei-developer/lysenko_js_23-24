define(function () {
  return {
    getItemVal: function (model, view) {
      let elem = document.getElementById('add-item')
      let self = this

      elem.onclick = function () {
        let text = document.getElementById('text-field')

        model.addItem(view, text.value) || self.refresh(model, view)
      }
    },

    doneTask: function (model, view) {
      let elems = document.getElementsByClassName('todos-item')
      let self = this

      for (let i in elems) {
        elems[i].onclick = function () {

          model.refresh(view, elems[i]) || self.refresh(model, view)
        }
      }
    },

    deleteTask: function (model, view) {
      let elems = document.getElementsByClassName('fa-times')
      let self =  this
      for (let i in elems) {
        elems[i].onclick = function () {

          model.remove(view, elems[i]) || self.refresh(model, view)
        }
      }
    },

    editTask: function (model, view) {
      let elems = document.getElementsByClassName('fa-pencil')
      let self = this

      for (let i in elems) {
        elems[i].onclick = function () {

          model.edit(view, elems[i]) || self.refresh(model, view)
        }
      }
    },

    init: function (model, view) {
      this.getItemVal(model, view)
    },

    refresh: function (model, view) {
      let self = this
      self.doneTask(model, view)
      self.deleteTask(model, view)
      self.editTask(model, view)
    }
  }
})
