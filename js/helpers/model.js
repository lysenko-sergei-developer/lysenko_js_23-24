define(function () {
  return {
    addItem: function (view, item) {
      view.renderСollectedItem(item)
    },

    refresh: function (view, item) {
      if (item.parentNode.nodeName == 'STRIKE') {
        view.removeStrike(item)
      } else {
        view.renderStrike(item)
      }
    },

    remove: function (view, item) {
      view.removeItem(item)
    },

    edit: function (view, item) {
      if (!item.parentNode.getElementsByTagName('strike')[0]) {
        view.editItem(item)
      }
    },
  }
})
