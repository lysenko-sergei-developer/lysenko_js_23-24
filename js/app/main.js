'use script';

define(function (require) {
  let model = require('js/helpers/model.js')
  let controller = require('js/helpers/controller.js')
  let view = require('js/helpers/view.js')

  controller.init(model, view)
})
